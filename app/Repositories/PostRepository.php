<?php

namespace App\Repositories;

use App\Models\Post;
use App\Models\Image;
use App\Interfaces\PostInterface;

class PostRepository implements PostInterface
{
    public function getPosts()
    {
        $query = Post::query();
        return $query;
    }

    public function getPostById($postId)
    {
        $query = $this->getPosts();
        return $query->findOrFail($postId);
    }

    public function createPost(array $postDetails)
    {
        $query = Post::create($postDetails);
        return $query;
    }

    public function updatePost($postId, array $newDetails)
    {
        $query = $this->getPostById($postId);
        $query->update($newDetails);
        return $this->getPostById($postId);
    }

    public function deletePost($postId)
    {
        $query = $this->getPostById($postId);
        return $query->delete();
    }

    public function addPostLike($postId)
    {
        $query = $this->getPostById($postId);
        $query->first();
        return $this->getPostById($postId);
    }

    public function getImages()
    {
        $query = Image::query();
        return $query;
    }

    public function addImages(array $imageDetails)
    {
        $query = Image::create($imageDetails);
        return $query;
    }

    public function getImageById($imageId)
    {
        $query = $this->getImages();
        return $query->findOrFail($imageId);
    }

    public function deleteImage($imageId)
    {
        $query = $this->getImageById($imageId);
        return $query->delete();
    }
}
