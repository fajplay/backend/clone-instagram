<?php

namespace App\Repositories;

use App\Interfaces\UserInterface;
use App\Models\Relationship;
use App\Models\User;

class UserRepository implements UserInterface
{
    public function getUsers()
    {
        $query = User::query();
        return $query;
    }

    public function getUserById($userId)
    {
        $query = $this->getUsers();
        return $query->findOrFail($userId);
    }

    public function createUser(array $userDetails)
    {
        $query = User::create($userDetails);
        return $query;
    }

    public function updateUser($userId, array $newDetails)
    {
        $query = $this->getUserById($userId);
        $query->update($newDetails);
        return $this->getUserById($userId);
    }

    public function deleteUser($userId)
    {
        $query = $this->getUserById($userId);
        return $query->delete();
    }

    public function getRelationships()
    {
        $query = Relationship::query();
        return $query;
    }
    public function getRelationshipsById($userId)
    {
        $query = $this->getRelationships();
        return $query->findOrFail($userId);
    }
    public function getFollowerById($userId)
    {
        $query = $this->getRelationships();
        $query->where('followed_id', $userId);
        return $query->get();
    }
    public function getFollowedById($userId)
    {
        $query = $this->getRelationships();
        $query->where('follower_id', $userId);
        return $query->get();
    }
    public function followUserCheck(array $following)
    {
        $query = $this->getFollowedById($following['follower_id']);
        $query->where('followed_id', $following['followed_id']);
        return $query->first();
    }
    public function followUser(array $following)
    {
        $check = $this->followUserCheck($following);
        $user = $this->getFollowerById($following['followed_id']);
        if ($check === null) {
            Relationship::created($following);
            return $user;
        } else {
            return $user;
        }
    }
    public function unfollowUser($followedId, $followerId)
    {
        $query = $this->getRelationships();
        $query->where('follower_id', $followerId);
        $query->where('followed_id', $followedId);
        return $query->delete();
    }
}
