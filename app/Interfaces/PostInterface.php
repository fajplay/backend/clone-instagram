<?php

namespace App\Interfaces;

interface PostInterface
{
    public function getPosts();
    public function getPostById($postId);
    public function createPost(array $postDetails);
    public function updatePost($postId, array $newDetails);
    public function deletePost($postId);
    public function addPostLike($postId);
    public function getImages();
    public function addImages(array $imageDetails);
    public function getImageById($imageId);
    public function deleteImage($imageId);
}
