<?php

namespace App\Interfaces;

interface UserInterface
{
    public function getUsers();
    public function getUserById($userId);
    public function createUser(array $userDetails);
    public function updateUser($userId, array $newDetails);
    public function deleteUser($userId);
    public function getRelationships();
    public function getRelationshipsById($userId);
    public function followUser(array $following);
    public function unfollowUser($followed_id, $follower_id);
    public function getFollowerById($userId);
    public function getFollowedById($userId);
}
