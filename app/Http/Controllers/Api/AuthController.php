<?php

namespace App\Http\Controllers\API;

use Throwable;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Interfaces\UserInterface;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private UserInterface $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(Request $request)
    {
        try {
            $this->validate($request, [
                'first_name' => 'required|min:4',
                'last_name' => 'required|min:4',
                'username' => 'required|min:4|unique:users',
                'email' => 'required|email|unique:users',
                'phone_number' => 'required|min:12|unique:users',
                'date_birth' => 'required|date',
                'password' => 'required|min:8',
            ]);

            $input = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'username' => $request->username,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'date_birth' => $request->date_birth,
                'password' => bcrypt($request->password)
            ];

            $query = $this->userRepository->createUser($input);
            $token = $query->createToken(env('TOKEN'))->accessToken;

            return response()->json(
                [
                    'token' => $token,
                    'data' => $query
                ],
                Response::HTTP_CREATED
            );

        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'email',
                'password' => 'min:8',
            ]);

            $input = [
                'email' => $request->email,
                'password' => $request->password
            ];

            if (auth()->attempt($input)) {
                $token = auth()->user()->createToken(env('TOKEN'))->accessToken;
                $id = Auth::id();
                $user = $this->userRepository->getUserById($id);
                return response()->json(
                    [
                        'token' => $token,
                        'data' => $user,
                    ],
                    Response::HTTP_ACCEPTED
                );
            } else {
                return response()->json(
                    ['error' => 'Unauthorised'],
                    Response::HTTP_UNAUTHORIZED
                );
            }

        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }
}
