<?php

namespace App\Http\Controllers\Api;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Interfaces\PostInterface;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    private PostInterface $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function indexPost(): JsonResponse
    {
        try {
            $posts = $this->postRepository->getPosts();
            return response()->json(
                ['posts' => $posts],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function storePost(Request $request): JsonResponse
    {
        try {
            $this->validate($request, [
                'caption' => 'required',
            ]);

            $userId = Auth::id();
            $postDetails = [
                'caption' => $request->caption,
                'comment' => $request->comment,
                'author_id' => $userId,
            ];

            $posts = $this->postRepository->createPost($postDetails);
            return response()->json(
                ['posts' => $posts],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function storeImages(Request $request): JsonResponse
    {
        try {
            $this->validate($request, [
                'caption' => 'required',
            ]);

            if(!$request->hasFile('images')) {
                return response()->json(['upload_file_not_found'], 400);
            }
            $userId = Auth::id();
            $postDetails = [
                'caption' => $request->caption,
                'comment' => $request->comment,
                'author_id' => $userId,
            ];

            $posts = $this->postRepository->createPost($postDetails);
            $postId = $posts->id;
                    foreach($request->images as $mediaFiles) {
                        $path = $mediaFiles->store('/images/resource', ['disk' => 'my_files']);
                        $imageDetails = [
                            'url' => $path,
                            'post_id' => $postId,
                        ];
                        $image[] = $this->postRepository->addImages($imageDetails);
                    }
            return response()->json(
                [
                    'posts' => $posts,
                    'image' => $image
                ],
                Response::HTTP_OK
            );

        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function updatePost(Request $request): JsonResponse
    {
        try {
            $postId = $request->route('postId');
            $post = $this->postRepository->getPostById($postId);
            $newDetails = [
                'caption' => $request->caption,
            ];
            $query = $this->postRepository->updatePost($postId, $newDetails);

            $userId = Auth::id();
            $postDetails = [
                'caption' => $request->caption,
                'comment' => $request->comment,
                'author_id' => $userId,
            ];

            $posts = $this->postRepository->createPost($postDetails);
            $postId = $posts->id;
                    foreach($request->images as $mediaFiles) {
                        $path = $mediaFiles->store('/images/resource', ['disk' => 'my_files']);
                        $imageDetails = [
                            'url' => $path,
                            'post_id' => $postId,
                        ];
                        $image[] = $this->postRepository->addImages($imageDetails);
                    }
            return response()->json(
                [
                    'posts' => $posts,
                    'image' => $image
                ],
                Response::HTTP_OK
            );

        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

}
