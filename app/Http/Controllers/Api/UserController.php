<?php

namespace App\Http\Controllers\Api;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Interfaces\UserInterface;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private UserInterface $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function showUserByAuth(): JsonResponse
    {
        try {
            $id = Auth::id();
            $user = $this->userRepository->getUserById($id);
            return response()->json(
                ['user' => $user],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function showUserById(Request $request): JsonResponse
    {
        try {
            $userId = $request->route('userId');
            $user = $this->userRepository->getUserById($userId);
            return response()->json(
                ['user' => $user],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function updateUserByAuth(Request $request): JsonResponse
    {
        try {
            $userId = Auth::id();
            $newDetails = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'username' => $request->username,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'date_birth' => $request->date_birth,
                'password' => bcrypt($request->password)
            ];
            $query = $this->userRepository->updateUser($userId, $newDetails);
            return response()->json(
                [
                    'data' => $query
                ],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function destroyByAuth(): JsonResponse
    {
        $userId = Auth::id();
        $this->userRepository->deleteUser($userId);

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    public function followUser(Request $request): JsonResponse
    {
        try {
            $followedId =$request->route('userId');
            $followerId = Auth::id();

            $input = [
                'followed_id' => $followedId,
                'follower_id' => $followerId,
            ];

            $user = $this->userRepository->followUser($input);

            return response()->json(
                ['user' => $user],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function unfollowUser(Request $request): JsonResponse
    {
        try {
            $followedId = $request->route('userId');
            $followerId = Auth::id();
            $user = $this->userRepository->unfollowUser(
                $followedId,
                $followerId
            );

            return response()->json(
                ['user' => $user],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function showFollower(): JsonResponse
    {
        try {
            $userId = Auth::id();
            $follower = $this->userRepository->getFollowerById($userId);
            return response()->json(
                ['follower' => $follower],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }

    public function showFollowed(): JsonResponse
    {
        try {
            $userId = Auth::id();
            $follower = $this->userRepository->getFollowedById($userId);
            return response()->json(
                ['following' => $follower],
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return response()->json([$e]);
        }
    }
}
