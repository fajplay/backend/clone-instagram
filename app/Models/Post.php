<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'caption',
        'comment',
        'like',
        'author_id',
    ];

    public function images()
    {
        return $this->hasMany('App\Image', 'product_id');
    }
}
