<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\AuthController as Auth;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\PostController;

Route::post('register', [Auth::class, 'register']);
Route::post('login', [Auth::class, 'login']);

Route::middleware('auth:api')->group(function () {
    Route::get('profile', [UserController::class, 'showUserByAuth']);
    Route::post('profile/update', [UserController::class, 'updateUserByAuth']);
    Route::delete('profile/delete', [UserController::class, 'destroyByAuth']);
    Route::get('profile/following', [UserController::class, 'showFollowed']);
    Route::get('profile/follower', [UserController::class, 'showFollower']);

    Route::get('profile/{userId}', [UserController::class, 'showUserById']);
    Route::post('profile/{userId}/follow', [UserController::class, 'followUser']);
    Route::post('profile/{userId}/unfollow', [UserController::class, 'unfollowUser']);

    Route::get('post', [PostController::class, 'indexPost']);
    Route::post('post', [PostController::class, 'storePost']);
    Route::post('post/add', [PostController::class, 'storeImages']);

});
